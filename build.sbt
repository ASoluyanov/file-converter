name := "home-assignment"

organization := "com.testing"

version := "1.0"

scalaVersion := "2.11.12"

assembly / assemblyMergeStrategy := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}

enablePlugins(JavaAppPackaging)
enablePlugins(DockerPlugin)

Compile / mainClass := Some("com.assignment.takehomeassignment.Assignment")

val versions = new {
  val scalaTestVersion = "3.0.5"
  val config = "1.2.1"
  val circe = "0.9.1"
  val circeConfig = "0.4.1"
  val scalaLogging = "3.8.0"
  val scopt = "3.7.0"
  val xml = "1.0.2"
  val logger = "3.9.4"
}

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % versions.scalaTestVersion % Test,
  "com.typesafe" % "config" % versions.config,
  "io.circe" %% "circe-generic" % versions.circe,
  "io.circe" %% "circe-parser" % versions.circe,
  "io.circe" %% "circe-config" % versions.circeConfig,
  "com.typesafe.scala-logging" %% "scala-logging" % versions.scalaLogging,
  "com.github.scopt" %% "scopt" % versions.scopt,
  "org.scala-lang.modules" %% "scala-xml" % versions.xml,
  "com.typesafe.scala-logging" %% "scala-logging" % versions.logger,
  "org.slf4j" % "slf4j-simple" % "1.6.4",
  "org.slf4j" % "slf4j-api" % "1.7.5"
)
