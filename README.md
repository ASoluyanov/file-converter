# Engineering assignment for central Data & Search teams

## Task

In this repository you'll find two files: _Workbook2.csv_ and _Workbook2.prn_. These files need to be converted to an HTML format by the code you deliver. Please consider your work as a proof of concept for a system that can keep track of credit limits from several sources.

## Instructions

To build uber jar run `sbt assembly`. You will find this file in target/scala-2.11 directory.
To convert files, run application:
`java -jar <fileName.jar> -s <source_file_name> -t <target_file_name>`
