package com.assignment.takehomeassignment

import org.scalatest._
import Assignment.{convertFileToHTML, getFileExtension}
import InputFilesProcessing.{processCsvRow, processPrnRow}
import scala.io.Source
import scala.xml.Utility.trim

class AssignmentTest extends WordSpec with Matchers {

  "Assignment" should {

    "define file extension" in {
      val fileExtension = getFileExtension("/some.test/path/filename.csv").get
      fileExtension shouldBe "csv"
    }

    "process a string of csv file" in {

      val csvFileString =
        """
          "John Doe",AAA,BBB,123
          |""".stripMargin

      val expectedOutput = <tr>
        <td>
          &quot;
          John Doe
          &quot;
        </td> <td>
          AAA
        </td> <td>
          BBB
        </td> <td>
          123
        </td>
      </tr>

      trim(processCsvRow(csvFileString)).toString shouldBe trim(expectedOutput).toString
    }

    "process a string of prn file" in {

      val prnFileString = "\"John Doe\"  AAA  BBB  123"

      val expectedOutput = <tr>
        <td>
          &quot;
          John\u00a0Doe
          &quot;
          \u00a0\u00a0AAA\u00a0\u00a0BBB\u00a0\u00a0123
        </td>
      </tr>

      trim(processPrnRow(prnFileString)).toString shouldBe trim(expectedOutput).toString

    }

    "throw exception if file not supported" in {

      val inputFilesLocation = getClass.getResource("/input_files").getPath

      an [java.lang.Exception] should be thrownBy
      convertFileToHTML("txt", Source.fromFile(s"$inputFilesLocation/Workbook2.txt"), "fake_target_path").get
    }
  }
}
