package com.assignment.takehomeassignment

import com.typesafe.scalalogging.LazyLogging
import InputFilesProcessing._
import scala.io.{BufferedSource, Source}
import scala.util.control.NonFatal
import scala.util.Try
import ConvertInstances._
import Convert._
import com.assignment.takehomeassignment.config.Parameters

object Assignment extends App with LazyLogging {

  val process = for {
    parameters <- Parameters.parse(args)
    fileExtension <- getFileExtension(parameters.source)
    bufferedSource <- Try(Source.fromFile(parameters.source))
    _ <- convertFileToHTML(fileExtension, bufferedSource, parameters.target)
    _ <- Try {
      bufferedSource.close
    }
  } yield {
    logger.info("File processing completed")
  }

  process.recover {
    case NonFatal(e) =>
      logger.error("Processing failed", e)
      sys.exit(1)
  }.get

  def getFileExtension(fileName: String): Try[String] = Try(fileName.split('.').last)

  /**
   * This method defines how to process input file based on it's extension
   */
  def convertFileToHTML(extension: String, fileContent: BufferedSource, target: String): Try[Unit] = Try {

    extension match {
      case "csv" => convertToHtml(Csv(fileContent), target)
      case "prn" => convertToHtml(Prn(fileContent), target)
      case _ => throw new Exception("file not supported")
    }
  }
}
