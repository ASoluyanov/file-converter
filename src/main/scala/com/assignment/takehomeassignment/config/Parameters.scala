package com.assignment.takehomeassignment.config

import scopt.OptionParser
import scala.util.{Failure, Success, Try}

case class Parameters(source: String = "", target: String = "")

object Parameters {

  def parse(args: Array[String]): Try[Parameters] = {

    new OptionParser[Parameters]("source file") {

      opt[String]('s', "source")
        .required()
        .valueName("<source>")
        .action((x, c) => c.copy(source = x))
        .text("Source location")

      opt[String]('t', "target")
        .required()
        .valueName("<target>")
        .action((x, c) => c.copy(target = x))
        .text("Target location")
    }.parse(args, Parameters()) match {
      case Some(x) => Success(x)
      case None =>
        Failure(new IllegalArgumentException("Error parsing arguments : " + args.mkString("[", ", ", "]")))
    }
  }
}
