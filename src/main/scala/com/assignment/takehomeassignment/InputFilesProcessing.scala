package com.assignment.takehomeassignment

import com.typesafe.scalalogging.LazyLogging
import scala.xml.Elem
import java.io._
import scala.io.BufferedSource

object InputFilesProcessing extends LazyLogging {

  val header: Elem = <head>
    <title>FileToHTML</title>
    <style type="text/css">
      td {{background-color:#ddddff; }}
    </style>
  </head>

  /**
   * This method adds header to the output html file.
   *
   * @param content input file content
   * @param target  target output file
   * @param f       function required to process rows of different file types
   */

  def processContentAndWrite(content: BufferedSource, target: String, f: String => Elem): Unit = {
    val outputFile = new File(target)
    val bw = new BufferedWriter(new FileWriter(outputFile))
    bw.write(s"<html>${header.toString()}<body><table>")
    content.getLines.foreach { line => bw.write(f(line).toString()) }
    bw.write("</table></body></html>")
    bw.close()
  }

  def processCsvRow(text: String): Elem = <tr>
    {text.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1).map(_.trim).map(s => <td>
      {s}
    </td>)}
  </tr>

  def processPrnRow(text: String): Elem = {
    <tr>
      <td>
        {" ".r.replaceAllIn(text, "\u00a0")}
      </td>
    </tr>
  }

  sealed trait InputFiles

  final case class Csv(content: BufferedSource) extends InputFiles

  final case class Prn(content: BufferedSource) extends InputFiles

  trait Convert[A] {
    def convertToHtml(a: A, target: String): Unit
  }

  object ConvertInstances {

    implicit val convertCsvToHtml: Convert[Csv] = new Convert[Csv] {

      def convertToHtml(csv: Csv, t: String): Unit = processContentAndWrite(csv.content, t, processCsvRow)
    }

    implicit val convertPrnToHtml: Convert[Prn] = new Convert[Prn] {
      def convertToHtml(prn: Prn, target: String): Unit = {

        processContentAndWrite(prn.content, target, processPrnRow)
      }
    }
  }

  object Convert {
    def convertToHtml[A](a: A, t: String)(implicit convertInstance: Convert[A]): Unit = {
      convertInstance.convertToHtml(a, t)
    }
  }


}
